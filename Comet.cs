﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Comet : MonoBehaviour {
	private static float COMET_RADIUS = 1.0f;
	private static float COMET_SPEED;

	public GameObject cometParticle;

	private float lastTime;
	private float waitTime = 0.02f;

	// Update is called once per frame
	void Update () {
		COMET_SPEED = 0.2f * PlayerController.PLAYER_SPEED;

		//add particles
		if (Time.time - lastTime > waitTime) {
			float angle = Random.Range (0, 2 * Mathf.PI);
			float radius = Random.Range (0, COMET_RADIUS);
			GameObject p = (GameObject)Instantiate (
				cometParticle,
				transform.TransformPoint (new Vector3(
					radius * Mathf.Cos(angle),
					radius * Mathf.Sin(angle),
					0
				)),
				Quaternion.identity
			);
			p.transform.SetParent (transform);
			lastTime = Time.time;
		}


		if (Camera.main.WorldToScreenPoint (transform.position).x < Screen.width) {
			transform.position += new Vector3 (0, 0, -COMET_SPEED * Time.deltaTime);
		}

		if (Camera.main.WorldToScreenPoint (transform.position).x < -Screen.width) {
			Destroy (this.gameObject);
		}
	}
}
