﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {
	public static bool ALIVE = true;

	private Camera cam;
	private Vector3 camOffset;
	private float inter = 0.5f;

	public static float MAX_SPEED = 30.0f;
	public static float STARTING_SPEED = 12.0f;
	public static float PLAYER_SPEED = STARTING_SPEED;
	public static float R = 0.05f;

	private float jumpSpeed = 18.0f; //18
	private float fallMultiplier = 3.2f;
	private float lowJumpMultiplier = 3.0f;

	Rigidbody rb;

	public GameObject standing;
	public GameObject ducking;

	private float lastTime;

	private int minSwipeLength = 30;
	private Vector2 startTouch;

	private bool isDucking = false;
	private static float DUCK_TIME = 0.5f;
	private static float DROP_TIME = 1.0f;
	private static float HOLD_DUCK_TIME = 0.05f;
	private float duckWaitingTime = 0.0f;
	private float duckTimeCounter = 0.0f;

	private BoxCollider bc;

    private AudioController audioController;

	public GameObject AdControllerObject;
	private AdController ac;

	// Use this for initialization
	void Start () {
		cam = Camera.main;
		camOffset = cam.transform.position - transform.position;

		rb = GetComponent<Rigidbody> ();

		standing.SetActive (true);
		ducking.SetActive (false);

		lastTime = Time.time;

		bc = GetComponent<BoxCollider> ();

        audioController = GameObject.FindGameObjectWithTag("Audio").GetComponent<AudioController>();

		ac = AdControllerObject.GetComponent<AdController> ();
	}

	// Update is called once per frame
	void Update () {

	}

	void setStand(bool b) {
		standing.SetActive (b);
		ducking.SetActive (!b);
		if (b) {
			bc.center = new Vector3 (0, 1.3f, 0);
			bc.size = new Vector3 (1, 5, 1);
		} else {
			bc.center = new Vector3 (0, 0.5f, 0);
			bc.size = new Vector3 (1, 3.5f, 1);
		}
	}

	void LateUpdate () {
		if (Input.GetMouseButtonDown(0)) {
			startTouch = Input.mousePosition;
		}
		if (Input.GetMouseButtonUp(0)) {
			float h = Input.mousePosition.y - startTouch.y;
			if (Mathf.Abs(h) > minSwipeLength) {
				if (h > 0 && GroundDetection.TOUCHING_GROUND) {	//jump
					//reset to standing
					rb.velocity = Vector3.up * jumpSpeed;
					isDucking = false;
					setStand (true);
                    audioController.playJump();
				} else if (h < 0) {		//duck
					//set to ducking
					isDucking = true;
					duckTimeCounter = Time.time;
					if (GroundDetection.TOUCHING_GROUND) {
						duckWaitingTime = DROP_TIME;
					} else {
						duckWaitingTime = DUCK_TIME;
					}
					setStand (false);
				}
			}
		}

		if (Input.GetMouseButton(0) && GroundDetection.TOUCHING_GROUND) {
			setStand (false);
			isDucking = true;
			duckTimeCounter = Time.time;
			duckWaitingTime = HOLD_DUCK_TIME;
		}

		if (isDucking) {
			if (!GroundDetection.TOUCHING_GROUND)
				rb.velocity += 2 * Vector3.up * Physics.gravity.y;
			if (Time.time - duckTimeCounter > duckWaitingTime) {
				isDucking = false;
				//return to standing
				setStand(true);
			}
		}

		transform.position += Vector3.forward * PLAYER_SPEED * Time.deltaTime;

		if (rb.velocity.y < 0) {
			rb.velocity += Vector3.up * Physics.gravity.y * (fallMultiplier) * Time.deltaTime;
		} else if (rb.velocity.y > 0 && !Input.GetKeyDown(KeyCode.Space)) {
			rb.velocity += Vector3.up * Physics2D.gravity.y * (lowJumpMultiplier) * Time.deltaTime;
		}

		PLAYER_SPEED += (Time.deltaTime * R * PLAYER_SPEED * (MAX_SPEED - PLAYER_SPEED))/MAX_SPEED;

		if (transform.position.y < -10) {
			ALIVE = false;
		}
		if (!ALIVE) {
			death ();
		}

		cam.transform.position = Vector3.Lerp (cam.transform.position, transform.position + camOffset, inter);
	}

	void death() {
		PLAYER_SPEED = STARTING_SPEED;
		setStand (true);
		for(int i = 0; i < transform.GetChild(0).transform.childCount; i++) {
			GameObject g = transform.GetChild (0).GetChild (i).gameObject;
			g.AddComponent<BoxCollider> ();
			g.AddComponent<Rigidbody> ();
		}
		Destroy(GetComponent<PlayerParticlesController>());
		Animator an = GameObject.FindGameObjectWithTag("Panel").GetComponent<Animator>();
		an.Play ("PanelDrop");

        audioController.playExplosion();

		ac.StartCoroutine ("ShowAd");

		Destroy (this);
	}
}
