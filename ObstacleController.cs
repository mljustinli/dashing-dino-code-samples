﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : MonoBehaviour {

	private GameObject player;
	bool passed = false;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {
		if (player.transform.position.z > transform.position.z && !passed) {
			passed = true;
			ScoreController.OBSTACLES_PASSED++;
		}
	}

	void OnTriggerEnter(Collider col) {
		if (col.tag.Equals("Player")) {
			PlayerController.ALIVE = false;
		}
	}
}
