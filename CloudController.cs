﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudController : MonoBehaviour {
	private static float CLOUD_Y = 12;
	private static float CLOUD_X_RANGE = 20; // 10 units from center line of x = 0

	private static float WIDTH_MIN = 3;
	private static float WIDTH_MAX = 5;
	private static float LENGTH_MIN = 6;
	private static float LENGTH_MAX = 8;
	private static float HEIGHT = 1;

	public GameObject cloudParent;

	private GameObject cloudPrefab;
	private GameObject player;

	private float lastTime;
	private float waitTime = 0.5f;

	// Use this for initialization
	void Start () {
		cloudPrefab = Resources.Load ("Prefabs/Cloud") as GameObject;
		player = GameObject.FindGameObjectWithTag ("Player");

		lastTime = Time.time;
	}

	// Update is called once per frame
	void Update () {
		if (Time.time - lastTime > waitTime) {
			GameObject g = (GameObject)Instantiate (cloudPrefab, new Vector3 (), Quaternion.identity);
			g.transform.position = new Vector3 (
				Random.Range(-CLOUD_X_RANGE, CLOUD_X_RANGE),
				CLOUD_Y,
				player.transform.position.z + 80
			);
			g.transform.localScale = new Vector3 (
				Random.Range(WIDTH_MIN, WIDTH_MAX),
				HEIGHT,
				Random.Range(LENGTH_MIN, LENGTH_MAX)
			);
			g.transform.SetParent (cloudParent.transform);

			lastTime = Time.time;
		}
	}
}
