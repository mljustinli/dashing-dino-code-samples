﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerParticles : MonoBehaviour {
	private static float START_WIDTH = 0.4f;
	private static float END_WIDTH = 1.6f;

	private static float START_HUE_MIN = 4.0f; //all out of 255
	private static float START_HUE_MAX = 38.0f;
	private static float START_SAT = 255.0f; // sat is for the S and V in HSV
	private static float SAT_CHANGE = 14.0f;

	private MeshRenderer mr;

	private float lastTime;
	private float waitTime = 0.05f;

	private float currWidth = START_WIDTH;

	// Use this for initialization
	void Start () {
		transform.localEulerAngles = new Vector3 (Random.Range (0, 360), Random.Range (0, 360), Random.Range (0, 360));
		transform.localScale = new Vector3 (START_WIDTH, START_WIDTH, START_WIDTH);

		mr = GetComponent<MeshRenderer> ();
		mr.material.color = Color.HSVToRGB (Random.Range (START_HUE_MIN, START_HUE_MAX)/255.0f, START_SAT/255.0f, START_SAT/255.0f);

		lastTime = Time.time;
	}

	// Update is called once per frame
	void Update () {
		mr.material.color -= Color.HSVToRGB (0, SAT_CHANGE/255.0f, SAT_CHANGE/255.0f);
		transform.position += new Vector3 (0, Random.Range(0, 6.0f) * Time.deltaTime, PlayerController.PLAYER_SPEED/4 * Time.deltaTime);

		if (Time.time - lastTime > waitTime) {
			if (currWidth < END_WIDTH) {
				currWidth += 0.05f;
				transform.localScale = new Vector3 (currWidth, currWidth, currWidth);
			}
			if (currWidth > END_WIDTH) {
				Destroy (this.gameObject);
			}
		}
	}
}
