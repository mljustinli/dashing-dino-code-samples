﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerParticlesController : MonoBehaviour {
	public static float FEET_WIDTH = 1.0f; // from center to one foot

	public GameObject footParticle;
	public GameObject particleParent;

	private float lastTime;
	private float waitTime = 0.02f;

	// Use this for initialization
	void Start () {
		lastTime = Time.time;
	}

	// Update is called once per frame
	void Update () {
		//add new particle
		if (Time.time - lastTime > waitTime) {
			//add new particle
			GameObject p1 = (GameObject) Instantiate(footParticle, transform.TransformPoint(new Vector3(FEET_WIDTH, -0.9f, -0.9f)), Quaternion.identity);
			GameObject p2 = (GameObject) Instantiate(footParticle, transform.TransformPoint(new Vector3(-FEET_WIDTH, -0.9f, -0.9f)), Quaternion.identity);
			p1.transform.SetParent (particleParent.transform);
			p2.transform.SetParent (particleParent.transform);
			lastTime = Time.time;
		}
	}
}
