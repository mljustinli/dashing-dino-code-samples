﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {
    private AudioController audioController;

	private void Start() {
        audioController = GameObject.FindGameObjectWithTag("Audio").GetComponent<AudioController>();
	}

	public void Restart() {
		PlayerController.ALIVE = true;
        ScoreController.SCORE = 0;
        audioController.playSelect();
		SceneManager.LoadScene (SceneManager.GetActiveScene().name);
	}

    public void LoadScene(string scene) {
        audioController.playSelect();
		SceneManager.LoadScene (scene);
	}
}
