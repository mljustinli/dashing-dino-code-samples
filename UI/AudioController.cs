﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour {
    public static AudioController AC;

    private AudioSource audioSource;

    public AudioClip jump;
    public AudioClip land;
    public AudioClip explosion;
    public AudioClip select;

    private static string PREFS_PATH = "volume";
    public static bool MUSIC_ON;

	// Use this for initialization
	void Awake () {
        if (AC == null) {
            DontDestroyOnLoad(gameObject);
            AC = this;
        } else if (AC != this) {
            Destroy(gameObject);
        }
	}

    private void Start() {
        audioSource = GetComponent<AudioSource>();

        if (PlayerPrefs.HasKey(PREFS_PATH)) {
            MUSIC_ON = PlayerPrefs.GetInt(PREFS_PATH) == 1; // 1 is on, 0 is off
        } else {
            MUSIC_ON = true;
        }
        audioSource.volume = MUSIC_ON ? 1 : 0;
    }

	// Update is called once per frame
	void Update () {

	}

    public void switchVolume () {
        MUSIC_ON = !MUSIC_ON;
        audioSource.volume = MUSIC_ON ? 1 : 0;
        PlayerPrefs.SetInt(PREFS_PATH, MUSIC_ON ? 1 : 0);
        PlayerPrefs.Save();
    }

    public void playJump() {
        audioSource.PlayOneShot(jump, 2.0f);
    }

    public void playLand() {
        audioSource.PlayOneShot(land, 2.0f);
    }

    public void playExplosion() {
        audioSource.PlayOneShot(explosion, 1.0f);
    }

    public void playSelect() {
        audioSource.PlayOneShot(select, 1.0f);
    }
}
