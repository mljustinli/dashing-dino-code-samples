﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeController : MonoBehaviour {
    private AudioController audioController;

    public Sprite volumeOn;
    public Sprite volumeOff;

    private Image volumeImage;

	// Use this for initialization
	void Start () {
        audioController = GameObject.FindGameObjectWithTag("Audio").GetComponent<AudioController>();

        volumeImage = GetComponent<Button>().image;

        setSprite();
	}

    public void switchVolume() {
        audioController.playSelect();
        audioController.switchVolume();
        setSprite();
    }

    void setSprite() {
        volumeImage.sprite = AudioController.MUSIC_ON ? volumeOn : volumeOff;
    }
}
