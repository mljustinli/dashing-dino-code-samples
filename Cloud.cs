﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud : MonoBehaviour {
	private static float SPEED = 6;

	// Update is called once per frame
	void Update () {
		if (Camera.main.WorldToScreenPoint (transform.position).x < -Screen.width) {
			Destroy (this.gameObject);
		}

		transform.position += new Vector3 (0, 0, -SPEED * Time.deltaTime);
	}
}
