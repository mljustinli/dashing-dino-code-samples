﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockController : MonoBehaviour {
	public static float BLOCK_WIDTH = 4; //orig is 3

	public static float MIN_BLOCK_LENGTH = 50;
	public static float MAX_BLOCK_LENGTH = 100;
	public static float MIN_BLOCK_HEIGHT = 7; //orig is 4 for both
	public static float MAX_BLOCK_HEIGHT = 7;

    private static float COLLIDER_HEIGHT = 1;

	private MeshFilter meshFilter;
	private Mesh mesh;

	private float blockLength;
	private float blockHeight;

	private Vector3 p0, p1, p2, p3, p4, p5, p6;

	//Obstacles
	public GameObject treePrefab;
	public GameObject rockPrefab;
	public GameObject cometPrefab;
	public GameObject rock2Prefab;
	public GameObject rock3Prefab;
	public GameObject rock3RotPrefab;
	public GameObject rock4Prefab;
	public GameObject rock5Prefab;

	private Dictionary<Obstacle, GameObject> obstacles;
	private int startingGap = 3; // leaves a little room at the beginning of a block

	// Use this for initialization
	void Start () {
		treePrefab = Resources.Load ("Prefabs/Tree1") as GameObject;
		rockPrefab = Resources.Load ("Prefabs/Rock") as GameObject;
		cometPrefab = Resources.Load ("Prefabs/Comet") as GameObject;
		rock2Prefab = Resources.Load ("Prefabs/Rock2") as GameObject;
		rock3Prefab = Resources.Load ("Prefabs/Rock3") as GameObject;
		rock3RotPrefab = Resources.Load ("Prefabs/Rock3 Rotated") as GameObject;
		rock4Prefab = Resources.Load ("Prefabs/Rock4") as GameObject;
		rock5Prefab = Resources.Load ("Prefabs/Rock5") as GameObject;

		obstacles = new Dictionary<Obstacle, GameObject> ();
		obstacles.Add (new Obstacle (3, new float[] {1, 1.5f}, 30.0f, new float[] {-4.02f, 1.3f}, 5000), treePrefab); //prob, length
		float cometMin = 2.0f;
		float cometMax = 3.0f;
		float cometMaxLength = 40.0f;
		obstacles.Add (new Obstacle (3, new float[] {cometMin, cometMax}, cometMaxLength, new float[] {0, 2.0f}, 20000), cometPrefab);
		obstacles.Add (new Obstacle (3, new float[] {cometMin, cometMax}, cometMaxLength, new float[] {0, 5.0f}, 30000), cometPrefab);
		obstacles.Add (new Obstacle (2, new float[] {1.5f, 2.5f}, 20.0f, new float[] {0, 0}, 10000), rock2Prefab);
		obstacles.Add (new Obstacle (1, new float[] {1.5f, 2.5f}, 20.0f, new float[] {0, 0}, 20000), rock3Prefab);
		obstacles.Add (new Obstacle (1, new float[] {1.5f, 2.5f}, 20.0f, new float[] {0, 0}, 25000), rock3RotPrefab);
		obstacles.Add (new Obstacle (1, new float[] {1.5f, 2.5f}, 20.0f, new float[] {0, 0}, 30000), rock4Prefab);
		obstacles.Add (new Obstacle (4, new float[] {1.5f, 2.5f}, 20.0f, new float[] {0, 0}, 0), rock5Prefab);

		recreate ();
	}

	// Update is called once per frame
	void Update () {
		MIN_BLOCK_LENGTH = 2 * PlayerController.PLAYER_SPEED;
		MAX_BLOCK_LENGTH = 4 * PlayerController.PLAYER_SPEED;
	}

	private float setBlockLength() {
		return Random.Range (MIN_BLOCK_LENGTH, MAX_BLOCK_LENGTH);
	}

	private float setBlockHeight() {
		return Random.Range (MIN_BLOCK_HEIGHT, MAX_BLOCK_HEIGHT);
	}

	public float getBlockLength() {
		return blockLength;
	}

	public float getBlockHeight() {
		return blockHeight;
	}

	public Mesh getMesh() {
		return mesh;
	}

	public bool addBlock() {
		return Camera.main.WorldToScreenPoint (transform.TransformPoint (p0)).x < Screen.width;
	}

	public bool removeBlock() {
		return Camera.main.WorldToScreenPoint (transform.TransformPoint (p3)).x < 0;
	}

	public void recreate() {
		//remove all children
		foreach (Transform child in transform)
		{
			Destroy(child.gameObject);
		}


		//resets the block and moves it to new position so it can be recycled
		blockLength = setBlockLength();
		blockHeight = setBlockHeight();
		p0 = new Vector3(0, 0, 0);
		p1 = new Vector3(BLOCK_WIDTH,0,0);
		p2 = new Vector3(0, 0, blockLength);
		p3 = new Vector3(BLOCK_WIDTH, 0, blockLength);
		p4 = new Vector3 (0, -blockHeight, 0);
		p5 = new Vector3 (BLOCK_WIDTH, -blockHeight, 0);
		p6 = new Vector3 (BLOCK_WIDTH, -blockHeight, blockLength);

		meshFilter = GetComponent<MeshFilter>();

		mesh = meshFilter.sharedMesh;
		if (mesh == null){
			meshFilter.mesh = new Mesh();
			mesh = meshFilter.sharedMesh;
		}
		mesh.Clear();
		mesh.vertices = new Vector3[]{ //separate the vertices so there are sharper edges
			p0, p2, p1,
			p2, p3, p1,
			p1, p3, p5,
			p3, p6, p5,
			p4, p0, p5,
			p0, p1, p5,
		};
		mesh.triangles = new int[]{
			0, 1, 2,
			3, 4, 5,
			6, 7, 8,
			9, 10, 11,
			12, 13, 14,
			15, 16, 17,
		};

		mesh.RecalculateNormals();
		mesh.RecalculateBounds();

        Destroy(gameObject.GetComponent<BoxCollider>()); //recalculates mesh collider
        BoxCollider bc = gameObject.AddComponent<BoxCollider>();
        bc.size = new Vector3(bc.size.x, COLLIDER_HEIGHT, bc.size.z);
        bc.center = new Vector3(bc.center.x, -COLLIDER_HEIGHT / 2, bc.center.z);

		float currLength = blockLength;
		currLength -= startingGap;

		while (currLength > 0) {
			//go through dictionary to find ones that are long enough
			//find sum of relative probabilities, choose random number from that
			//get position of obstacles and spawn the gameobject

			bool haveOption = false; // meaning there is an obstacle that fits, otherwise do nothing
			int sum = 0;
			foreach(KeyValuePair<Obstacle, GameObject> entry in obstacles)
			{
				entry.Key.setLength (PlayerController.PLAYER_SPEED);	//new random length of gap
				if (currLength > entry.Key.getLength() && ScoreController.SCORE >= entry.Key.getScoreSpawn()) {
					haveOption = true;
					sum += entry.Key.getProb ();
				}
			}
			if (!haveOption) { //aka no obstacles fit
				break;
			} else {
				int randPos = Random.Range (0, sum);
				foreach(KeyValuePair<Obstacle, GameObject> entry in obstacles) {
					if (currLength > entry.Key.getLength() && ScoreController.SCORE >= entry.Key.getScoreSpawn()) {
						randPos -= entry.Key.getProb ();
						if (randPos < 0) {
							//spawn in this obstacle
							GameObject ob = (GameObject) Instantiate(
								entry.Value,
								transform.TransformPoint(new Vector3(
									BLOCK_WIDTH/2 + entry.Key.getOffset()[0],
									0 + entry.Key.getOffset()[1],
									p2.z - currLength + entry.Key.getLength()/2
								)),
								entry.Value.transform.rotation
							);
							ob.transform.parent = this.transform;
							currLength -= entry.Key.getLength ();
							break;
						}
					}
				}
			}
		}
	}
}

public class Obstacle {
	private int prob;		// relative probability of spawning
	private float[] range;	// minimum gap to spawn this obstacle
	private float length;		// current gap to spawn this obstacle
	private float maxLength;
	private float[] offset; // 2 numbers for x and y offset
	private int scoreSpawn; //when the obstacle starts spawning (when score is greater than scoreSpawn)

	public Obstacle (int prob, float[] range, float maxLength, float[] offset, int scoreSpawn) {
		this.prob = prob;
		this.range = range;
		this.length = range [0];
		this.maxLength = maxLength;
		this.offset = offset;
		this.scoreSpawn = scoreSpawn;
	}

	public int getProb() {
		return prob;
	}

	public void setLength(float speed) {
		length = Mathf.Min (Random.Range (range [0] * speed, range [1] * speed), maxLength);
	}

	public float getLength() {
		return length;
	}

	public float[] getOffset() {
		return offset;
	}

	public int getScoreSpawn() {
		return scoreSpawn;
	}
}
