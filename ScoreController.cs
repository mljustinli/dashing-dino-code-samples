﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreController : MonoBehaviour {
	public static int OBSTACLES_PASSED;
	public static int SCORE;

	public int score;

	public GameObject gameTMPOb;
	private TextMeshProUGUI gameTMP;
	public GameObject highTMPOb;
	private TextMeshProUGUI highTMP;

	public GameObject deathTMPOb;
	private TextMeshProUGUI deathTMP;

    public GameObject newRecordOb;

	public static string PREFS_PATH = "highscore";
	private int highscore;

	// Use this for initialization
	void Start () {
		OBSTACLES_PASSED = 0;

		score = 0;

		gameTMP = gameTMPOb.GetComponent<TextMeshProUGUI> ();
		highTMP = highTMPOb.GetComponent<TextMeshProUGUI> ();
		deathTMP = deathTMPOb.GetComponent<TextMeshProUGUI> ();

        newRecordOb.SetActive(false);

		highscore = getHighscore ();
	}

	// Update is called once per frame
	void Update () {
		if (PlayerController.ALIVE) {
			score += OBSTACLES_PASSED;
			gameTMP.text = "Score " + score;
		} else {
			checkHighscore ();
			deathTMP.text = "Score " + score;
			Destroy (this);
		}

		if (score > highscore) {
			highscore = score;
            if (!newRecordOb.activeSelf) {
                newRecordOb.SetActive(true);
            }
		}
		highTMP.text = "HI " + highscore;

		SCORE = score;
	}

	void checkHighscore() {
		int oldScore = getHighscore();
		if (score > oldScore) {
			PlayerPrefs.SetInt (PREFS_PATH, score);
			PlayerPrefs.Save ();
		}
	}

	int getHighscore() {
		if (PlayerPrefs.HasKey(PREFS_PATH)) {
			return PlayerPrefs.GetInt (PREFS_PATH);
		} else {
			return 0;
		}
	}
}
