﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSpawner : MonoBehaviour {
	private static float MIN_GAP;
	private static float MAX_GAP;

	public GameObject blockPrefab;
	public GameObject levelParent;

	public Material groundMaterial;

	private List<GameObject> bcsReserve;
	private List<GameObject> bcs; //like bc's for block controllers

	// Use this for initialization
	void Start () {
		bcsReserve = new List<GameObject> ();
		bcs = new List<GameObject> ();

		for (int i = 0; i < 7; i++) {
			addReserve ();
		}
		addBlock (new Vector3 (-BlockController.BLOCK_WIDTH / 2, 0, -5)); //starting block
	}

	// Update is called once per frame
	void Update () {
		MIN_GAP = 0.5f * PlayerController.PLAYER_SPEED;
		MAX_GAP = 0.7f * PlayerController.PLAYER_SPEED;

		GameObject firstBlock = bcs [0];
		BlockController firstbc = firstBlock.GetComponent<BlockController> ();

		GameObject lastblock = bcs [bcs.Count - 1];
		BlockController lastbc = lastblock.GetComponent<BlockController> ();

		if (firstbc.removeBlock()) {
			removeBlock ();
		}
		if (lastbc.addBlock()) {
			addBlock ();
		}
	}

	private void addReserve() {
		GameObject block = (GameObject)Instantiate (
			blockPrefab,
			new Vector3 (0, 0, -4 * BlockController.MAX_BLOCK_LENGTH),
			Quaternion.identity
		);
		block.GetComponent<MeshRenderer> ().enabled = false;
		block.GetComponent<MeshRenderer> ().material = groundMaterial;
		block.tag = "Ground";
		bcsReserve.Add (block);

		block.transform.parent = levelParent.transform;
	}

	private void addBlock(Vector3 pos) {
		GameObject block = bcsReserve [0];
		bcs.Add (block);
		bcsReserve.Remove(block);
		block.transform.position = pos;
		block.GetComponent<MeshRenderer> ().enabled = true;
	}

	private void addBlock() {
		if (bcsReserve.Count == 0) { //if not enough blocks
			addReserve ();
		}

		GameObject block = bcsReserve [0];

		BlockController bc = block.GetComponent<BlockController> ();
		bc.recreate ();

		GameObject prevBlock = bcs [bcs.Count - 1];
		BlockController prevbc = prevBlock.GetComponent<BlockController> ();
		block.transform.position = prevBlock.transform.position
			+ new Vector3 (
				0,
				0,
				prevbc.getBlockLength () + Random.Range(MIN_GAP, MAX_GAP)
			);

		bcs.Add (block);
		bcsReserve.Remove(block);
		block.GetComponent<MeshRenderer> ().enabled = true;
	}

	private void removeBlock() {
		GameObject block = bcs [0];
		bcsReserve.Add (block);
		bcs.Remove (block);
		block.GetComponent<MeshRenderer> ().enabled = false;
	}
}
