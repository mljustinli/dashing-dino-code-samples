﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundDetection : MonoBehaviour {
	public static bool TOUCHING_GROUND = true;

    private AudioController audioController;

	private void Start() {
        audioController = GameObject.FindGameObjectWithTag("Audio").GetComponent<AudioController>();
	}

	void OnTriggerEnter(Collider col) {
		if (col.tag.Equals("Ground")) {
			TOUCHING_GROUND = true;
            audioController.playLand();
		}
	}

	void OnTriggerExit(Collider col) {
		if (col.tag.Equals("Ground")) {
			TOUCHING_GROUND = false;
		}
	}
}
